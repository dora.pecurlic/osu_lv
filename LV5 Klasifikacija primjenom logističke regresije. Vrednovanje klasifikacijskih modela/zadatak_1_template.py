import numpy as np
import matplotlib
import matplotlib.pyplot as plt

from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split


X, y = make_classification(n_samples=200, n_features=2, n_redundant=0, n_informative=2,
                            random_state=213, n_clusters_per_class=1, class_sep=1)

# train test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)


#a) Prikažite podatke za ucenje u  x1 −x2 ravnini matplotlib biblioteke pri cemu podatke obojite 
#s obzirom na klasu. Prikažite i podatke iz skupa za testiranje, ali za njih koristite drugi
#marker (npr. ’x’). Koristite funkciju scatter koja osim podataka prima i parametre c i
#cmap kojima je moguce de ´ finirati boju svake klas



plt.scatter(x=X_train[:,0],y=X_train[:,1] , c = y_train,  cmap="coolwarm", marker="o")
plt.scatter(x=X_test[:,0],y=X_test[:,1] , c=y_test,cmap="coolwarm", marker="x")


#b) Izgradite model logisticke regresije pomocu scikit-learn biblioteke na temelju skupa podataka za ucenje.

from sklearn . linear_model import LogisticRegression
model = LogisticRegression()
model.fit(X_train, y_train )

#c()) Pronadite u atributima izgradenog modela parametre modela. Prikažite granicu odluke 
#naucenog modela u ravnini ˇ x1 − x2 zajedno s podacima za ucenje. Napomena: granica ˇ
#odluke u ravnini x1 −x2 definirana je kao krivulja: θ0 +θ1x1 +θ2x2 = 0.

Theta0 = model.intercept_
theta1 = model.coef_[0,0]
theta2 = model.coef_[0,1]

x_min = np.min(X_train[:, 1])
x_max = np.max(X_train[:,1])

x2 = np.linspace(x_min, x_max, 100)

x1 = -Theta0/theta1 - (theta2*x2)/theta1

plt.plot(x1,x2)
plt.fill_between(x1,x2,x_min, alpha = 0.2, color="blue")
plt.fill_between(x1,x2,x_max, alpha = 0.2, color="red")

plt.show()


#d) Provedite klasifikaciju skupa podataka za testiranje pomocu izgradenog modela logisticke 
#regresije. Izracunajte i prikažite matricu zabune na testnim podacima. Izracunate tocnost, 
#preciznost i odziv na skupu podataka za testiranje

from sklearn . metrics import confusion_matrix , ConfusionMatrixDisplay
from sklearn.metrics import classification_report, accuracy_score, precision_score, recall_score


y_test_p = model.predict(X_test)

cm = confusion_matrix(y_test, y_test_p)
print("Matrica zabune: ", cm)

disp = ConfusionMatrixDisplay(confusion_matrix(y_test, y_test_p))
disp.plot()
plt.show()

print(classification_report(y_test, y_test_p)) 
print("\n")

accuracy = accuracy_score(y_test, y_test_p)
precision = precision_score(y_test, y_test_p)
recall = recall_score(y_test, y_test_p)
print("Točnost: {:.2f}".format(accuracy))
print("Preciznost: {:.2f}".format(precision))
print("Odziv: {:.2f}".format(recall))

#e) Prikažite skup za testiranje u ravnini x1 −x2. Zelenom bojom oznacite dobro klasificirane
#primjere dok pogrešno klasificirane primjere oznacite crnom bojom

correctly_classified = (y_test == y_test_p)
incorrectly_classified = (y_test != y_test_p)

plt.scatter(X_test[correctly_classified, 0],X_test[correctly_classified, 1], c="green", marker = "o", label="correct tested data")
plt.scatter(X_test[incorrectly_classified, 0],X_test[incorrectly_classified, 1], c="black", marker = "x", label="incorrect tested data")

plt.show()
