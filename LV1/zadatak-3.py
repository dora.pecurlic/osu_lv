def mean(numbers):
    return sum(numbers)/len(numbers)

numbers=[]
while True:
    number = input("enter number or keyword-done: ")
    if number.isnumeric():
        numbers.append(int(number))
    elif number.lower() == "done":
        break
    else:
        print("Please enter a number!")

print(numbers)

print("List count: ", len(numbers))
print("Mean: ", mean(numbers))
print("Min value: ", min(numbers))
print("Max value: ", max(numbers))

numbers.sort()
print("Sorted list: ", numbers)