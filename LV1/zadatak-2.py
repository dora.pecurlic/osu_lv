try:
    grade = float(input("Enter number between 0.0 and 1.0: "))
    if(grade < 0.0 or grade > 1.0):
        print("Entered number is not in the range")
    elif(grade >= 0.9):
        print("A")
    elif(grade >= 0.8):
        print("B")
    elif(grade >= 0.7):
        print("C")
    elif(grade >= 0.6):
        print("D")
    elif(grade < 0.6):
        print("F")
except:
    print('Entered value must be number!')