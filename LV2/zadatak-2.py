
import numpy as np
import matplotlib.pyplot as plt
from numpy import genfromtxt


data = genfromtxt('data.csv', delimiter=',', skip_header=1)
print(data)

#a)Na temelju velicine numpy polja data, na koliko osoba su izvršena mjerenja?
print("Broj osoba: ", len(data))

#b)Prikažite odnos visine i mase osobe pomocu naredbe ´ matplotlib.pyplot.scatter.

plt.scatter(data[:,1], data[:,2])#izdvajanje drugog i treceg stupca
plt.show()

#c)Ponovite prethodni zadatak, ali prikažite mjerenja za svaku pedesetu osobu na slici

plt.scatter(data[::50,1], data[::50,2])#izdvajanje svakog pedesetog elementa drugog i treceg stupca
plt.show()

#d) Izracunajte i ispišite u terminal minimalnu, maksimalnu i srednju vrijednost visine u ovom podatkovnom skupu.

print("Minimalna vrijednost: ", np.min(data[:,1]))
print("Maksimalna vrijednost: ", np.max(data[:,1]))
print("Srednja  vrijednost: ", np.average(data[:,1]))

#e) min,max,avg za zene, i isto to za muskarce - odvojeno

m_ind = data[:,0]== 1
m_data = data[m_ind]
print("Minimalna vrijednost M: ", np.min(m_data[:,1]))
print("Maksimalna vrijednost M: ", np.max(m_data[:,1]))
print("Srednja  vrijednost M: ", np.average(m_data[:,1]))

z_ind = data[:,0]== 0
z_data = data[z_ind]
print("Minimalna vrijednost  Z: ", np.min(z_data[:,1]))
print("Maksimalna vrijednost Z: ", np.max(z_data[:,1]))
print("Srednja  vrijednost Z: ", np.average(z_data[:,1]))