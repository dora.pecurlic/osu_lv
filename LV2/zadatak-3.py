import matplotlib.pyplot as plt
import numpy as np
from PIL import Image, ImageEnhance


img = plt.imread('C:/Users/Dora_/Desktop/FERIT - 3. GODINA/OSU_LV/LV2/road.jpg')


plt.figure()
plt.title("Originalna slika")
plt.imshow(img, alpha=1)
plt.show()

#posvijetliti sliku možemo omoću alpha<1
plt.figure()
plt.title("Posvijetljena slika slika")
plt.imshow(img, alpha=0.5)
plt.show()

#prikaz druge četvrtine po širini
y_quarter = img.shape[1]// 4
y_half = img.shape[1]//2
img2 = img[: ,y_quarter:y_half,:]
plt.figure()
plt.title("Druga četvrtina slike po širini")
plt.imshow(img2)
plt.show()


#zarotirati sliku za 90 stupnjeva u smijeru kazaljke na satu
plt.figure()
plt.title("zarotirana slika za 90 u smjeru kazaljke")
plt.imshow(np.rot90(img, k=1)) #k=2 za 180, k=3 za 270 itd USMIJERU KAZALJKE
plt.show()

img_rot = np.rot90(img, 3)#ovo je zapravo u smijeru kazaljke
plt.figure()
plt.imshow(img_rot, cmap="gray")
plt.show()

#zrcaljenje slike
plt.figure()
plt.title("Zrcaljena slika")
img_mirror = np.fliplr(img)
plt.imshow(img_mirror)
plt.show()

#zrcaljenje slike horizontalno
plt.figure()
img_mirror = np.flipud(img)
plt.imshow(img_mirror)
plt.show()