import numpy as np
import matplotlib.pyplot as plt

#crna boja je predstavljana 0, a bijela 1
crna = np.zeros((50,50))
bijela = np.ones((50,50))

gore = np.hstack((crna, bijela)) #spajanje dvije slike horizontalno
dolje = np.hstack((bijela, crna))
slika = np.vstack((gore, dolje))#spajanje dvije slike vertikalno

plt.imshow(slika,cmap='gray')
plt.show()