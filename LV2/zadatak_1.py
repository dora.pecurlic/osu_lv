import matplotlib.pyplot as plt
import numpy as np

x = np.array([1,3,3,2,1], np.float32)
y = np.array([1,1,2,2,1], np.float32)

plt.plot(x,y,'g', linewidth=2, marker="o", markersize = 5)
plt.axis([0.0,4.0,0.0,4.0]) #granice x i y osi na grafu
plt.xlabel("X OS")
plt.ylabel("Y OS")
plt.title("ZADATAK 1")
plt.show()