import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as Image
from sklearn.cluster import KMeans

# ucitaj sliku
#img = Image.imread("imgs\\test_1.jpg")
img= Image.imread("C:/Users/Dora_/Desktop/FERIT - 3. GODINA/OSU_LV/LV7 Grupiranje podataka primjenom algoritma K srednjih vrijednosti-20230509/imgs/imgs/test_1.jpg")

# prikazi originalnu sliku
plt.figure()
plt.title("Originalna slika")
plt.imshow(img)
plt.tight_layout()
plt.show()

# pretvori vrijednosti elemenata slike u raspon 0 do 1
img = img.astype(np.float64) / 255

# transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
w,h,d = img.shape
img_array = np.reshape(img, (w*h, d))

# rezultatna slika
img_array_aprox = img_array.copy()

# a)Ova skripta ucitava originalnu RGB sliku  test_1.jpg
#te ju transformira u podatkovni skup koji dimenzijama odgovara izrazu (7.2) pri cemu je n
#broj elemenata slike, a m je jednak 3. Koliko je razlicitih boja prisutno u ovoj slici?

diff_colors = len(np.unique(img_array_aprox, axis=0))
print("Broj različitih boja: ", diff_colors)

#b) Primijenite algoritam K srednjih vrijednosti koji ce pronaci grupe u RGB vrijednostima elemenata originalne slike.

km = KMeans(n_clusters=10, init='random', n_init=5, random_state=0)
km.fit(img_array_aprox)
labels = km.predict(img_array_aprox)


#C) Vrijednost svakog elementa slike originalne slike zamijeni s njemu pripadajucim centrom

img_array_aprox = km.cluster_centers_[labels]
img_aprox = np.reshape(img_array_aprox, (w,h,d))    #povratak na originalnu dimenziju slike 

#d). Usporedite dobivenu sliku s originalnom. Mijenjate broj grupa K. Komentirajte dobivene rezultate

plt.figure()
plt.title("Nova slika")
plt.imshow(img_aprox)
plt.tight_layout()
plt.show()
#povecavanjem broja grupa, vise je boja, slika sve vise izgleda kao originalna, smanjivanjem manje boja i sve dalje od originala
#duljina izvodenja programa se bitno povecava s povecanjem broja grupa

#e) Graficki prikažite ovisnost J o broju grupa K. Koristite atribut inertia objekta klase KMeans. Možete li uociti lakat koji upucuje na optimalni broj grupa?
#LAKAT METODA
plt.figure()
for i in range(1, 9):
    km = KMeans(n_clusters=i, init='random', n_init=5, random_state=0)
    km.fit(img_array_aprox)
    plt.plot(i, km.inertia_,".-r", linewidth=2)
    plt.xlabel("K")
    plt.ylabel("j")
    plt.title("LAKAT METODA")
plt.show()

#f)Elemente slike koji pripadaju jednoj grupi prikažite kao zasebnu binarnu sliku. Što primjecujete?

labels_unique = np.unique(labels)

for i in range(len(labels_unique)):
    binary_image = labels == labels_unique[i]
    binary_image = np.reshape(binary_image,(h,w)) #potrebno reshapeat za prikaz nazad u normalne dimenzije slike(bez rgb dimenzije)
    plt.figure()
    plt.title(f"Binarna slika {i+1}. grupe boja")
    plt.imshow(binary_image)
    plt.tight_layout
    plt.show()


'''
MATKOVO
clusters = 4
for i in range(clusters):
    bit_values = labels==[i]
    binary_img = np.reshape(bit_values, (img.shape[0:2]))
    binary_img = binary_img*1
    x=int(i/2)
    y=i%2
    plt.imshow(binary_img)
    plt.show()
'''