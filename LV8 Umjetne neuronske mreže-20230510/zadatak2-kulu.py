import numpy as np
from tensorflow import keras
from keras import layers
from matplotlib import pyplot as plt

(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

model = keras.models.load_model("FCN/model.keras")

# skaliranje slike na raspon [0,1]
x_train_s = x_train.astype("float32") / 255
x_test_s = x_test.astype("float32") / 255

x_train_s = x_train_s.reshape((60000, 784))
x_test_s = x_test_s.reshape((10000, 784))

y_pred = model.predict(x_test_s)
y_pred_labels = np.argmax(y_pred, axis=1)

misclassified_idx = np.where(y_pred_labels != y_test)[0]

fig, axes = plt.subplots(nrows=3, ncols=3, figsize=(8, 8))
for i, ax in enumerate(axes.flat):
    idx = misclassified_idx[i]
    ax.imshow(x_test[idx], cmap="gray")
    ax.set_title(f"True: {y_test[idx]} Pred: {y_pred_labels[idx]}")
    ax.axis("off")

plt.show()

'''
Ovaj kod prikazuje slike koje su pogrešno klasificirane od strane modela za MNIST dataset.

Najprije se učitavaju podaci iz MNIST dataset-a, a zatim se model koji je treniran na ovim podacima učitava iz prethodno spremljenog fajla. Nakon toga, podaci se skaliraju na raspon [0, 1] i mijenjaju oblik kako bi odgovarali ulaznom obliku modela.

Zatim se poziva model.predict() funkcija na testnim podacima kako bi se dobili predviđeni izlazi za svaku sliku. Korištenjem np.argmax() funkcije, dobivaju se predviđene labele za svaku sliku.

Sljedeća linija koda koristi np.where() funkciju kako bi se dobili indeksi slika koje su pogrešno klasificirane, odnosno one slike čija je predviđena labela različita od stvarne.

Konačno, petlja prolazi kroz prvih 9 pogrešno klasificiranih slika i prikazuje ih u 3x3 mreži, zajedno sa stvarnom i predviđenom labelom.



'''