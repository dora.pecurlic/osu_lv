import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
import sklearn.linear_model as lm
from sklearn.metrics import (
    mean_absolute_error,
    mean_squared_error,
    mean_absolute_percentage_error,
    r2_score,
)
from sklearn.preprocessing import OneHotEncoder
from sklearn.compose import ColumnTransformer


data = pd.read_csv('data_C02_emission.csv')

input_variables = [
    'Fuel Consumption City (L/100km)',
    'Fuel Consumption Hwy (L/100km)',
    'Fuel Consumption Comb (L/100km)',
    'Fuel Consumption Comb (mpg)',
    'Engine Size (L)',
    'Fuel Type',
    'Cylinders',
]

output_variable =[ "CO2 Emissions (g/km)"]

ohe = OneHotEncoder()
X_encoded = ohe.fit_transform(data[['Fuel Type']]).toarray()
data['Fuel Type'] = X_encoded
#print(data.dtypes)

X = data[input_variables]
y = data[output_variable]


X_train , X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1)

linear_model = lm.LinearRegression()

linear_model.fit(X_train, y_train)

print(linear_model.coef_)

y_test_p = linear_model.predict(X_test)


MSE = mean_squared_error(y_test, y_test_p, squared=True)
print('MSE:',MSE)

RMSE = mean_squared_error(y_test, y_test_p, squared=False)
print('RMSE:',RMSE)

MAE = mean_absolute_error(y_test, y_test_p)
print('MAE:',MAE)

MAPE = mean_absolute_percentage_error(y_test, y_test_p)
print('MAPE:',MAPE*100,'%')

r2 = r2_score(y_test, y_test_p)
print('r2:',r2)


X_test_2 = X_test[0:100]
y_test_2 = y_test[0:100]
y_test_2_p = linear_model.predict(X_test_2)

print('\nWITH LESS LEARNING DATA:')
MSE = mean_squared_error(y_test_2, y_test_2_p, squared=True)
print('MSE:',MSE)

RMSE = mean_squared_error(y_test_2, y_test_2_p, squared=False)
print('RMSE:',RMSE)

MAE = mean_absolute_error(y_test_2, y_test_2_p)
print('MAE:',MAE)

MAPE = mean_absolute_percentage_error(y_test_2, y_test_2_p)
print('MAPE:',MAPE*100,'%')

r2 = r2_score(y_test_2, y_test_2_p)
print('r2:',r2)

#NA TEMELJU METRIKA ZA EVALUACIJIU REGRESIJSKIH MODELA
#POGRESKA JE MALA
plt.show()
