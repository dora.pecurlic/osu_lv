import pandas as pd
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
import sklearn.linear_model as lm
from sklearn.metrics import (
    mean_absolute_error,
    mean_squared_error,
    mean_absolute_percentage_error,
    r2_score,
)
#1.KORAK: priprema podataka

data = pd.read_csv('data_C02_emission.csv')

print("types:\n", data.dtypes)

input_variables = [
    'Fuel Consumption City (L/100km)',
    'Fuel Consumption Hwy (L/100km)',
    'Fuel Consumption Comb (L/100km)',
    'Fuel Consumption Comb (mpg)',
    'Engine Size (L)',
    'Cylinders',
]

output_variable = ["CO2 Emissions (g/km)"]

X = data[input_variables].to_numpy()
y = data[output_variable].to_numpy()

X_train , X_test , y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1)

#b) Pomocu matplotlib biblioteke i dijagrama raspršenja prikažite ovisnost emisije C02 plinova
#o jednoj numerickoj velicini. Pri tome podatke koji pripadaju skupu za ucenje oznacite
#plavom bojom, a podatke koji pripadaju skupu za testiranje oznacite crvenom bojom.

plt.figure()
plt.scatter(X_train[:,0], y_train, color = "blue", label = "Training data")
plt.scatter(X_test[:,0], y_test, color = "red",  label = "Testing data")
plt.xlabel("Fuel Consumption City (L/100km)")
plt.ylabel("CO2 Emissions (g/km)")
plt.legend()
plt.show()

#c)Izvršite standardizaciju ulaznih velicina skupa za ucenje. Prikažite histogram vrijednosti
#jedne ulazne velicine prije i nakon skaliranja. Na temelju dobivenih parametara skaliranja
#transformirajte ulazne velicine skupa podataka za testiranje

sc = MinMaxScaler()

X_train_n = sc.fit_transform(X_train)


plt.figure()
plt.title("Pre-scaling training data")
plt.hist(X_train[:,0])
plt.show()

plt.figure()
plt.title("Post-scaling training data")
plt.hist(X_train_n[:,0])
plt.show()


#ovo je rjesenje sa sub plotom da se mogu usporedit bolje
ax1 = plt.subplot(211)
ax1.hist(x=X_train[:, 0])
ax1.set_title("Pre-scaling training data")
ax2 = plt.subplot(212)
ax2.hist(x=X_train_n[:, 0])
ax2.set_title("Post-scaling training data")
plt.show()


X_test_n = sc.transform(X_test)

plt.figure()
plt.title("Pre-scaling test data")
plt.hist(X_test[:,0])
plt.show()

plt.figure()
plt.title("Post-scaling test data")
plt.hist(X_test_n[:,0])
plt.show()

#d) Izgradite linearni regresijski modeli. Ispišite u terminal dobivene parametre modela i
#povežite ih s izrazom 4.6.

linear_model = lm.LinearRegression()
linear_model.fit(X_train_n, y_train)
print(linear_model.coef_)

#e) Izvršite procjenu izlazne velicine na temelju ulaznih velicina skupa za testiranje. Prikažite
#pomocu dijagrama raspršenja odnos izmedu stvarnih vrijednosti izlazne velicine i procjene
#dobivene modelom

y_test_p = linear_model.predict(X_test_n)

plt.figure()
plt.scatter(x=y_test, y=y_test_p)
plt.xlabel("Real values")
plt.ylabel("Predicted  values")
plt.show()

#f) Izvršite vrednovanje modela na nacin da izracunate vrijednosti regresijskih metrika na
#skupu podataka za testiranje.

MSE = mean_squared_error(y_test, y_test_p, squared=True)
RMSE = mean_squared_error(y_test, y_test_p, squared=False)
MAE = mean_absolute_error(y_test, y_test_p)
MAPE = mean_absolute_percentage_error(y_test, y_test_p)
R2 = r2_score(y_test, y_test_p)

print("MSE: ", MSE)
print("RMSE: ", RMSE)
print("MAE: ", MAE)
print("MAPE: ", MAPE*100 ,"%")
print("R2: ", R2)


#g) Što se dogada s vrijednostima evaluacijskih metrika na testnom skupu kada mijenjate broj ulaznih velicina?

X_test_n_2 = X_test_n[0:100] #izdvajanje prvih 100 primjeraka
y_test_2 = y_test[0:100]
y_test_2_p = linear_model.predict(X_test_n_2)

print('\nWITH LESS LEARNING DATA:')
MSE = mean_squared_error(y_test_2, y_test_2_p, squared=True)
print('MSE:',MSE)

RMSE = mean_squared_error(y_test_2, y_test_2_p, squared=False)
print('RMSE:',RMSE)

MAE = mean_absolute_error(y_test_2, y_test_2_p)
print('MAE:',MAE)

MAPE = mean_absolute_percentage_error(y_test_2, y_test_2_p)
print('MAPE:',MAPE*100,'%')

r2 = r2_score(y_test_2, y_test_2_p)
print('r2:',r2)

plt.show()

#Smanjivanjem broja ulaznih podataka povecava se pogreska
