import pandas as pd
import matplotlib.pyplot as plt

data = pd.read_csv('C:/Users/Dora_/Desktop/FERIT - 3. GODINA/OSU_LV/LV3/data_C02_emission.csv')

#a) Pomocu histograma prikažite emisiju C02 plinova. Komentirajte dobiveni prikaz

plt.figure()
plt.title("Emisija CO2")
data['CO2 Emissions (g/km)'].plot(kind='hist', bins = 20)
plt.show()


#b) Pomocu dijagrama raspršenja prikažite odnos izmedu gradske potrošnje goriva i emisije 
#C02 plinova. Kako biste bolje razumjeli odnose izmedu
#velicina, obojite tockice na dijagramu raspršenja s obzirom na tip goriva

data['Fuel Color'] = data['Fuel Type'].map(
    {
        "X": "Red",
        "Z": "Blue",
        "D": "Green",
        "E": "Purple",
        "N": "Yellow",
    }
)

data.plot.scatter(x='Fuel Consumption City (L/100km)', y='CO2 Emissions (g/km)', c='Fuel Color')
plt.show()

#c) Pomocu kutijastog dijagrama prikažite razdiobu izvangradske potrošnje s obzirom na tip
#goriva. Primjecujete li grubu mjernu pogrešku u podacima?

data.boxplot(column=['Fuel Consumption Hwy (L/100km)'], by='Fuel Type')
plt.show()

#Pomocu stupcastog dijagrama prikažite broj vozila po tipu goriva. Koristite metodu groupby

new_data = data.groupby("Fuel Type")['Make'].count().plot(kind="bar")
plt.xlabel("Tip goriva")
plt.ylabel("Broj vozila")
plt.title("Broj vozila po tipu goriva")
plt.show()

#e) Pomocu stupcastog grafa prikažite na istoj slici prosjecnu C02 emisiju vozila s obzirom na broj cilindara

data.groupby('Cylinders')['CO2 Emissions (g/km)'].mean().plot(kind='bar', ax=new_data, color ='orange')
plt.show()