import pandas as pd
import numpy as np


data = pd.read_csv('C:/Users/Dora_/Desktop/FERIT - 3. GODINA/OSU_LV/LV3/data_C02_emission.csv')

#a) Koliko mjerenja sadrzi DF, kojeg je tipa svaka velicina, kategoriscke convertiraj u tip category

print("DF sadrzi mjerenja: ",len(data))
print('\n')

print("types:\n", data.dtypes)
print('\n')

print("Broj izostalih vrijednosti: \n", data.isnull().sum())
print('\n')
print("Broj dupliciranih vrijednosti: ", data.duplicated().sum())

data.drop_duplicates()
data.reset_index(drop=True)


for col in data:
    if data[col].dtype == object:
        data[col]=data[col].astype('category')



print("types:\n", data.dtypes)

#b)Koja tri automobila ima najvecu odnosno najmanju gradsku potrošnju? Ispišite u terminal: 
#ime proizvoda¯ ca, model vozila i kolika je gradska potrošnja.

sorted = data.sort_values(by='Fuel Consumption City (L/100km)', ascending=False )
print("Automobili s najvecom potrosnjom: \n")
print(sorted[['Make','Model', 'Fuel Consumption City (L/100km)']].head(3))
print("\n Automobili s najmanjom potrosnjom: \n")
print(sorted[['Make','Model', 'Fuel Consumption City (L/100km)']].tail(3))

#c)Koliko vozila ima velicinu motora između 2.5 i 3.5 L? Kolika je prosjecna C02 emisija plinova za ova vozila?

engineSize_data = data[ (data['Engine Size (L)'] >= 2.5 ) & (data['Engine Size (L)'] <= 3.5 )]
print("Koliko vozila ima velicinu motora između 2.5 i 3.5 L? ", len(engineSize_data))
print("Prosjecna emisija CO2 za njih: ", engineSize_data['CO2 Emissions (g/km)'].mean())

#d)Koliko mjerenja se odnosi na vozila proizvodaca Audi? Kolika je prosjecna emisija C02
#plinova automobila proizvodaca Audi koji imaju 4 cilindara?

audi = data[ data['Make'] == 'Audi']
audi_4C = audi[audi['Cylinders'] == 4]
print("Audi: ", len(audi))
print("Prosjecna emisija CO2 audija s 4 cilindra: ",audi_4C['CO2 Emissions (g/km)'].mean())

#e)Koliko je vozila s 4,6,8. . . cilindara? Kolika je prosjecna emisija C02 plinova s obzirom na broj cilindara?

cyl_data = data[data['Cylinders'] %2 == 0]
print("Broj vozila s parnim brojem cilindara: ", len(cyl_data))
print("Prosjecna emisija CO2 plinova: ", cyl_data['CO2 Emissions (g/km)'].mean())

#f)Kolika je prosjecna gradska potrošnja u slucaju vozila koja koriste dizel, a kolika za vozila 
#koja koriste regularni benzin? Koliko iznose medijalne vrijednosti?

dizel = data[data['Fuel Type'] == 'D']
regular = data[data['Fuel Type'] == 'X']
print("Prosjecna vrijednost gradske potrosnje DIZEL: ", dizel['Fuel Consumption City (L/100km)'].mean())
print("Medijan vrijednost gradske potrosnje DIZEL: ", dizel['Fuel Consumption City (L/100km)'].median())
print("\n")
print("Prosjecna vrijednost gradske potrosnje BENZIN: ", regular['Fuel Consumption City (L/100km)'].mean())
print("Medijan vrijednost gradske potrosnje BENZIN: ", regular['Fuel Consumption City (L/100km)'].median())

#g)Koje vozilo s 4 cilindra koje koristi dizelski motor ima najvecu gradsku potrošnju goriva?

dizel_4C = data[ (data['Cylinders']  == 4) & (data['Fuel Type'] == 'D')]
print("Vozilo s 4 cilindra i dizelskim motorom - max potrosnje: ", dizel_4C['Fuel Consumption City (L/100km)'].max())

#h) Koliko ima vozila ima rucni tip mjenjaca (bez obzira na broj brzina)?

manual = data[ data['Transmission'].str.startswith("M")]
print("Manual: ", len(manual))

#i) Izracunajte korelaciju između numerickih velicina. Komentirajte dobiveni rezultat.

print("Korelacija: ", data.corr(numeric_only=True))